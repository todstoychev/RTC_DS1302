#!/usr/bin/python

# Use the class for the DS1302 RTC chip.
import RTC_DS1302
import os

# Create an instance of the RTC class.
rtc = RTC_DS1302.RTC_DS1302()

# Functions to write to the RTC chip.
date_string = os.popen('date +"%y %m %d %u %H %M %S"').read()

rtc.write_ram('Last set: ' + os.popen('date +"%Y-%m-%d %H:%M:%S"').read().replace("\n", ""))

date_array = date_string.split()

rtc.write_date_time(int(date_array[0]), int(date_array[1]), int(date_array[2]), int(date_array[3]), int(date_array[4]),
                    int(date_array[5]), int(date_array[6]))
print('Date set was: ' + date_string)

date_time = {"Year": 0, "Month": 0, "Day": 0, "DayOfWeek": 0, "Hour": 0, "Minute": 0, "Second": 0}
data = rtc.read_date_time(date_time)
print('Date read was: ' + data)

# Finish with the Raspberry Pi GPIO pins.
rtc.close_GPIO()
