Library to get DS1302 real time clock to run on Raspberry PI Zero. It is also compatible with other versions of RPI, but you will need to rewire pins.

Setup is based on [Kevin Saye's article](https://kevinsaye.wordpress.com/2017/12/22/adding-an-ds1302-to-a-raspberry-pi-zero-w/).

Code is based on [Json Birch's version](http://www.newsdownload.co.uk/pages/RTC_DS1302.html).

# Setup

## Pinout 

| DS1302 pin | RPI pin |
|------------|---|
| VCC | 1 |
| Ground | 6 |
| CLK | 13 |
| Dat | 12 |
| RST | 11 |
