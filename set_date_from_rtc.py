#!/usr/bin/python
 
# Use the class for the DS1302 RTC chip.
import RTC_DS1302
import os
 
# Create an instance of the RTC class.
rtc = RTC_DS1302.RTC_DS1302()
 
# Functions to read from the RTC chip.
print("RAM: " + rtc.read_ram())
 
date_time = {"Year": 0, "Month": 0, "Day": 0, "Hour": 0, "Minute": 0, "Second": 0}
data = rtc.read_date_time(date_time)
print("RTC: " + data)

date_set_response = os.popen("date -s \"" + data + "\"").read()
 
print("Time has been set to: " + date_set_response)
 
# Finish with the Raspberry Pi GPIO pins.
rtc.close_GPIO()
