# RTC_DS1302 - Python Hardware Programming Education Project For Raspberry Pi
# Copyright (C) 2015 Jason Birch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# The program was updated by Todor Todorov, to be compatible with Python 3.
# For contacts todstoychev@gmail.com.

# /****************************************************************************/
# /* RTC_DS1302                                                               */
# /* ------------------------------------------------------------------------ */
# /* V1.00 - 2015-08-26 - Jason Birch                                         */
# /* V2.00 - 2022-04-04 - Todor Todorov                                       */
# /* ------------------------------------------------------------------------ */
# /* Class to handle controlling a Real Time Clock IC DS1302.                 */
# /****************************************************************************/

import time
import operator
import RPi.GPIO


class RTC_DS1302:
    __RTC_DS1302_SCLK = 27
    __RTC_DS1302_CE = 17
    __RTC_DS1302_IO = 18

    __CLK_PERIOD = 0.00001

    __DOW = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    def __init__(self):
        # Turn off GPIO warnings.
        RPi.GPIO.setwarnings(False)
        # Configure Raspberry Pi GPIO interfaces.
        RPi.GPIO.setmode(RPi.GPIO.BCM)
        # Initiate DS1302 communication.
        self.initiate()
        # Make sure write protect is turned off.
        self.write_byte(int("10001110", 2))
        self.write_byte(int("00000000", 2))
        # Make sure trickle charge mode is turned off.
        self.write_byte(int("10010000", 2))
        self.write_byte(int("00000000", 2))
        # End DS1302 communication.
        self.end()

    def close_GPIO(self):
        """Close Raspberry Pi GPIO use before finishing."""
        RPi.GPIO.cleanup()

    def initiate(self):
        """Start a transaction with the DS1302 RTC."""
        RPi.GPIO.setup(self.__RTC_DS1302_SCLK, RPi.GPIO.OUT, initial=0)
        RPi.GPIO.setup(self.__RTC_DS1302_CE, RPi.GPIO.OUT, initial=0)
        RPi.GPIO.setup(self.__RTC_DS1302_IO, RPi.GPIO.OUT, initial=0)
        RPi.GPIO.output(self.__RTC_DS1302_SCLK, 0)
        RPi.GPIO.output(self.__RTC_DS1302_IO, 0)
        time.sleep(self.__CLK_PERIOD)
        RPi.GPIO.output(self.__RTC_DS1302_CE, 1)

    def end(self):
        """Complete a transaction with the DS1302 RTC"""
        RPi.GPIO.setup(self.__RTC_DS1302_SCLK, RPi.GPIO.OUT, initial=0)
        RPi.GPIO.setup(self.__RTC_DS1302_CE, RPi.GPIO.OUT, initial=0)
        RPi.GPIO.setup(self.__RTC_DS1302_IO, RPi.GPIO.OUT, initial=0)
        RPi.GPIO.output(self.__RTC_DS1302_SCLK, 0)
        RPi.GPIO.output(self.__RTC_DS1302_IO, 0)
        time.sleep(self.__CLK_PERIOD)
        RPi.GPIO.output(self.__RTC_DS1302_CE, 0)

    def write_byte(self, byte):
        """Write a byte of data to the DS1302 RTC."""
        for count in range(8):
            time.sleep(self.__CLK_PERIOD)
            RPi.GPIO.output(self.__RTC_DS1302_SCLK, 0)

            bit = operator.mod(byte, 2)
            byte = operator.floordiv(byte, 2)
            time.sleep(self.__CLK_PERIOD)
            RPi.GPIO.output(self.__RTC_DS1302_IO, bit)

            time.sleep(self.__CLK_PERIOD)
            RPi.GPIO.output(self.__RTC_DS1302_SCLK, 1)

    def read_byte(self):
        """Read a byte of data to the DS1302 RTC."""
        RPi.GPIO.setup(self.__RTC_DS1302_IO, RPi.GPIO.IN, pull_up_down=RPi.GPIO.PUD_DOWN)

        byte = 0

        for count in range(8):
            time.sleep(self.__CLK_PERIOD)
            RPi.GPIO.output(self.__RTC_DS1302_SCLK, 1)

            time.sleep(self.__CLK_PERIOD)
            RPi.GPIO.output(self.__RTC_DS1302_SCLK, 0)

            time.sleep(self.__CLK_PERIOD)
            bit = RPi.GPIO.input(self.__RTC_DS1302_IO)
            byte |= ((2 ** count) * bit)

        return byte

    def write_ram(self, data):
        """Write a message to the RTC RAM."""
        # Initiate DS1302 communication.
        self.initiate()
        # Write address byte.
        self.write_byte(int("11111110", 2))

        # Write data bytes.
        for count in range(len(data)):
            self.write_byte(ord(data[count:count + 1]))

        for count in range(31 - len(data)):
            self.write_byte(ord(" "))

        # End DS1302 communication.
        self.end()

    def read_ram(self):
        """Read message from the RTC RAM."""
        # Initiate DS1302 communication.
        self.initiate()
        # Write address byte.
        self.write_byte(int("11111111", 2))
        # Read data bytes.
        data = ""

        for count in range(31):
            byte = self.read_byte()
            data += chr(byte)

        # End DS1302 communication.
        self.end()

        return data

    def write_date_time(self, year, month, day, day_of_week, hour, minute, second):
        """Write date and time to the RTC."""
        # Initiate DS1302 communication.
        self.initiate()
        # Write address byte.
        self.write_byte(int("10111110", 2))
        # Write seconds data.
        self.write_byte(operator.mod(second, 10) | operator.floordiv(second, 10) * 16)
        # Write minute data.
        self.write_byte(operator.mod(minute, 10) | operator.floordiv(minute, 10) * 16)
        # Write hour data.
        self.write_byte(operator.mod(hour, 10) | operator.floordiv(hour, 10) * 16)
        # Write day data.
        self.write_byte(operator.mod(day, 10) | operator.floordiv(day, 10) * 16)
        # Write month data.
        self.write_byte(operator.mod(month, 10) | operator.floordiv(month, 10) * 16)
        # Write day of week data.
        self.write_byte(operator.mod(day_of_week, 10) | operator.floordiv(day_of_week, 10) * 16)
        # Write year of week data.
        self.write_byte(operator.mod(year, 10) | operator.floordiv(year, 10) * 16)
        # Make sure write protect is turned off.
        self.write_byte(int("00000000", 2))
        # Make sure trickle charge mode is turned off.
        self.write_byte(int("00000000", 2))
        # End DS1302 communication.
        self.end()

    def read_date_time(self, date_time):
        """Read date and time from the RTC."""
        # Initiate DS1302 communication.
        self.initiate()
        # Write address byte.
        self.write_byte(int("10111111", 2))
        # Read date and time data.
        data = ""

        byte = self.read_byte()
        date_time["Second"] = operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10
        byte = self.read_byte()
        date_time["Minute"] = operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10
        byte = self.read_byte()
        date_time["Hour"] = operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10
        byte = self.read_byte()
        date_time["Day"] = operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10
        byte = self.read_byte()
        date_time["Month"] = operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10
        byte = self.read_byte()
        date_time["DayOfWeek"] = (operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10) - 1
        byte = self.read_byte()
        date_time["Year"] = operator.mod(byte, 16) + operator.floordiv(byte, 16) * 10

        data = self.__DOW[date_time["DayOfWeek"]] + " " + format(date_time["Year"] + 2000, "04d") + "-" + format(
            date_time["Month"], "02d") + "-" + format(date_time["Day"], "02d")
        data += " " + format(date_time["Hour"], "02d") + ":" + format(date_time["Minute"], "02d") + ":" + format(
            date_time["Second"], "02d")

        # End DS1302 communication.
        self.end()

        return data
